package com.sahanbcs.basicsockert.impliment.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientOne {

    public static void main(String[] args) {
        InetAddress address=null;
        try {
            address= InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        Socket socket = null;
        String req = null;
        String res = null;
        BufferedReader is = null;
        PrintWriter os = null;

        try {
            socket = new Socket(address,4000);
            is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            os = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO Exception");
        }

        System.out.println("Client Address : "+address);
//        req = "Sahan Bcs\n";
//        req = "PU\n";
        req = "PY\n";
        System.out.println("Enter Data to echo Server  :" + req);




        try {
            os.println(req);
            os.flush();
            res = is.readLine();
            System.out.println("Server Response : "+res);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Socket read Error");
        }finally {

            try {
                if(is !=null){
                    is.close();
//                    System.out.println("Socket InputStream Closed");
                }
                if(os !=null){
                    os.close();
//                    System.out.println("Socket OutputStream Closed");
                }
                if(socket!=null){
                    socket.close();
//                    System.out.println("Socket Closed");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

}

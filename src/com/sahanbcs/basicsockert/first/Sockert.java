package com.sahanbcs.basicsockert.first;

import javax.net.ServerSocketFactory;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Sockert {


    private static final int PORT_NUM = 5000;
    public static void main(String args[]) {
        ServerSocketFactory serverSocketFactory =
                ServerSocketFactory.getDefault();
        ServerSocket serverSocket = null;
        try {
            serverSocket =
                    serverSocketFactory.createServerSocket(PORT_NUM);
        } catch (IOException ignored) {
            System.err.println("Unable to create server");
            System.exit(-1);
        }
        System.out.printf("LogServer running on port: %s%n", PORT_NUM);
        while (true) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
                InputStream is = socket.getInputStream();
                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(is, "US-ASCII"));
                        new InputStreamReader(is,  StandardCharsets.UTF_8));
                PrintWriter  writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
                String line = null;
                while ((line = br.readLine() ) != null) {
                    System.out.println(line);
                    writer.println("sahan Res");
                    writer.flush();
                }

            } catch (IOException exception) {
                // Just handle next request.
//                System.out.println("Error  :" + exception.getMessage() );
//                exception.printStackTrace();

            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
    }

}

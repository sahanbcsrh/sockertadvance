package com.sahanbcs.basicsockert.first;


import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;

public class LogTest
{
    private static Logger logger = Logger.getAnonymousLogger();

    public static void main(String argv[]) throws IOException
    {
        InetAddress add = InetAddress.getLocalHost();
        Handler handler = new SocketHandler(add.getHostAddress(), 5000);
        logger.addHandler(handler);
        logger.log(Level.SEVERE, "Hello, World");
        logger.log(Level.SEVERE, "Welcome Home");
        logger.log(Level.SEVERE, "Hello, World");
        logger.log(Level.SEVERE, "Welcome Home");
    }
}